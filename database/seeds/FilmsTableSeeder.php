<?php

use Illuminate\Database\Seeder;

class FilmsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $genres = \App\Genre::all();
        factory(App\Film::class, 3)->create()->each(function ($film) use($genres) {
            $film->comments()->save(factory(\App\Comment::class)->make());
            $film->genres()->sync(
                $genres->random(rand(1, 3))->pluck('id')->toArray()
            );
        });
    }
}
