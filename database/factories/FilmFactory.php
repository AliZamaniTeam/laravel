<?php

use Faker\Generator as Faker;

$factory->define(\App\Film::class, function (Faker $faker) {
    $name = $faker->jobTitle;
    return [
        'name'         => $name,
        'slug'         => str_slug($name),
        'description'  => $faker->realText(100),
        'release_date' => $faker->date(),
        'rating'       => $faker->numberBetween(1, 5),
        'ticket_price' => $faker->randomNumber(),
        'country'      => $faker->country,
        'photo'        => $faker->randomElement(['1.jpg', '2.jpg', '3.jpg'])
    ];
});
