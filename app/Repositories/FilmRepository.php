<?php

namespace App\Repositories;

use App\Film;

class FilmRepository extends EloquentRepository implements RepositoryInterface
{

    protected $model;

    public function __construct() {
        $this->model = $this->getModel();
    }

    public function getModel() {
        return new Film();
    }

    public function create($data) {
        return $this->model->create($data);
    }
    
}