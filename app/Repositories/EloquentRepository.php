<?php

namespace App\Repositories;

abstract class EloquentRepository
{

    /**
     * Create and fill new
     * 
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(array $data)
    {
        $this->model->fill($data);
        $this->model->save();
        return $this->model;
    }

    /**
     * Update with data
     * 
     * @param array $data
     * @param integer $id
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function update($id, array $data)
    {
        $model = $this->model->find($id);
        $model->fill($data);
        $model->save();
        return $model;
    }
    
    public function delete($id)
    {
        $model = $this->model->find($id);
        $model->delete();
    }

}