<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    public $fillable = ['name'];
    public $timestamps = false;

    public function films(){
        return $this->belongsToMany(Film::class);
    }
}
