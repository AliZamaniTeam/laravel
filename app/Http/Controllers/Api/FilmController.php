<?php

namespace App\Http\Controllers\Api;

use App\Film;
use App\Http\Resources\FilmCollection;
use App\Http\Controllers\Controller;

class FilmController extends Controller
{
    public function index(){
        return new FilmCollection(Film::with(['genres'])->paginate(1));
    }

    public function show($slug){
        $film = Film::where('slug', $slug)->with(['genres', 'comments' => function ($q) {
            return $q->orderBy('id', 'desc');
        }])->first();

        if($film) return new \App\Http\Resources\Film($film);
        else return array();
    }

}
