<?php

namespace App\Http\Controllers\Api;

use App\Genre;
use App\Http\Resources\GenreCollection;
use App\Http\Controllers\Controller;

class GenreController extends Controller
{
    public function index(){
        return new GenreCollection(Genre::all());
    }
}
