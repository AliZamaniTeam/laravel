<?php

namespace App\Http\Controllers;

use App\Film;
use App\Http\Requests\AddFilmCommentRequest;
use App\Http\Requests\AddFilmRequest;
use App\Services\FilmService;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class FilmController extends Controller
{
    private $filmService;

    public function __construct(FilmService $filmService){
        $this->filmService = $filmService;
    }

    public function index(Request $request){
        $input = $request->all();
        $films = $this->filmService->getFilms($input);
        return view('films.index', compact('films'));
    }

    public function show($slug){
        $film = $this->filmService->getFilm($slug);
        return view('films.show', compact('film'));
    }

    public function create(){
        $genres = $this->filmService->getGenres();
        return view('films.create', compact('genres'));
    }

    public function store(AddFilmRequest $request){
        $newFilmSlug = $this->filmService->storeFilm($request);
        return redirect()->route('web.film', ['slug' => $newFilmSlug]);
    }

    public function storeComment(AddFilmCommentRequest $request, $slug){
        $input = $request->all();
        $newCommentId = $this->filmService->storeFilmComment($input, $slug);
        return redirect()->route('web.film', ['slug' => $slug, '#comment-' . $newCommentId]);
    }
}
