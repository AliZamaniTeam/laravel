<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddFilmRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'         => 'required',
            'description'  => 'required',
            'release_date' => 'required|date_format:Y-m-d',
            'rating'       => 'required|between:1,5',
            'ticket_price' => 'required|integer',
            'genres'       => 'required|array',
            'country'      => 'required',
            'photo'        => 'required|mimes:jpg,jpeg,gif,png,bmp',
        ];
    }
}
