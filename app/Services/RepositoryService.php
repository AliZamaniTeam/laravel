<?php

namespace App\Services;

use Exception;
use Illuminate\Database\DatabaseManager;
use App\Repositories\EloquentRepository;

abstract class RepositoryService
{
    /**
     *
     * @var DatabaseManager
     */
    private $database;

    /**
     *
     * @var EloquentRepository
     */
    private $repository;

    public function __construct(DatabaseManager $database, EloquentRepository $repository)
    {
        $this->database     = $database;
        $this->repository   = $repository;
    }

    public function create($data)
    {
        $this->database->beginTransaction();

        try {
            $model = $this->repository->create($data);
        } catch (Exception $e) {
            $this->database->rollBack();
            throw $e;
        }

        $this->database->commit();

        return $model;
    }

    public function update($id, $data)
    {
        $this->database->beginTransaction();

        try {
            $model = $this->repository->update($id, $data);
        } catch (Exception $e) {
            $this->database->rollBack();
            throw $e;
        }
        $this->database->commit();

        return $model;
    }

    public function delete($id)
    {
        $this->database->beginTransaction();

        try {
            $this->repository->delete($id);
        } catch (Exception $e) {
            $this->database->rollBack();
            throw $e;
        }

        $this->database->commit();
    }


}