<?php

namespace App\Services;

use App\Repositories\FilmRepository;
use App\Services\RepositoryService;
use GuzzleHttp\Client;
use Illuminate\Database\DatabaseManager;

class FilmService extends RepositoryService
{
    private $database;
    private $client;
    private $filmRepository;

    public function __construct(DatabaseManager $database, FilmRepository $filmRepository, Client $client)
    {
        $this->client           = $client;
        $this->database         = $database;
        $this->filmRepository   = $filmRepository;
    }

    public function getFilms($input)
    {
        $page = isset($input['page']) ? $input['page'] : 1;
        $response = $this->client->request('GET', route('api.films', ['page' => $page]));
        return json_decode($response->getBody()->getContents());
    }

    public function getFilm($input)
    {
        $response = $this->client->request('GET', route('api.film', ['slug' => $input]));
        $contents = $response->getBody()->getContents();
        if(isset(json_decode($contents)->data)) return json_decode($contents)->data;
        else return false;
    }

    public function getGenres()
    {
        $response = $this->client->request('GET', route('api.genres'));
        return json_decode($response->getBody()->getContents())->data;
    }

    public function storeFilm($request)
    {
        $photo = $request->file('photo');
        $fileName = time() . '.' . $photo->getClientOriginalExtension();
        $photo->storeAs('films_cover', $fileName);
        $input = $request->all();
        $input['photo'] = $fileName;

        $film =$this->filmRepository->create($input);
        $film->genres()->sync($request->genres);

        return $film->slug;
    }

    public function storeFilmComment($input, $slug){
        $film = $this->filmRepository->getModel()->where('slug', $slug)->first();
        $comment = $film->comments()->create($input);
        return $comment->id;
    }
}