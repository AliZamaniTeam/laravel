<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public $fillable = ['name', 'comment'];

    public function film(){
        return $this->belongsTo(Film::class);
    }
}
