<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    public $fillable = ['name', 'slug', 'description', 'release_date', 'rating', 'ticket_price', 'country', 'photo'];

    public function genres(){
        return $this->belongsToMany(Genre::class);
    }

    public function comments(){
        return $this->hasMany(Comment::class);
    }

    /**
     * Set the slug field.
     *
     * @param  string  $value
     * @return void
     */
    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = str_slug($this->attributes['name']);
    }
}
