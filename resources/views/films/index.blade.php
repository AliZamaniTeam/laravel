@extends('layouts.app')
@section('content')
    <div class="container text-center">
        @if(empty($films->data))
            <h2 style="color:orangered">Oops! Not any results</h2>
            <a href="{{ route('web.films') }}">Back to life</a>
        @else
            @foreach($films->data as $film)
                <a href="{{ route('web.film', $film->slug) }}"><img src="uploads/films_cover/{{ $film->photo }}" class="rounded mx-auto d-block img-thumbnail mb-4 w-25"></a>
                <h2>{{ $film->name }}</h2>
                <hr>
                <strong class="badge badge-warning">Rating: </strong> {{ $film->rating }}from 5
                <p class="mt-4">{{ $film->description }}</p>
                <hr>
                @if(isset($films->links->prev))
                    <a href="{{ route('web.films', ['page' => $films->meta->current_page - 1]) }}" class="btn btn-warning pull-right">← Previous</a>
                @endif
                @if(isset($films->links->next))
                    <a href="{{ route('web.films', ['page' => $films->meta->current_page + 1]) }}" class="btn btn-warning pull-left">Next →</a>
                @endif
            @endforeach
        @endif
    </div>

@stop


