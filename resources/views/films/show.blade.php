@extends('layouts.app')
@section('content')
    <div class="container text-center">
        @if(empty($film))
            <h2 style="color:orangered">Oops! Not any results</h2>
            <a href="{{ route('web.films') }}">Back to life</a>
        @else
            <a href="{{ route('web.film', $film->slug) }}" title="Click for details"><img src="../uploads/films_cover/{{ $film->photo }}" class="rounded mx-auto d-block img-thumbnail mb-4 w-25"></a>
            <h2>{{ $film->name }}</h2>
            <hr>
            <strong class="badge badge-warning">Rating: </strong> {{ $film->rating }} from 5 •
            <strong>Release date: </strong>{{ $film->release_date }} •
            <strong>Country: </strong>{{ $film->country }} <br><br>
            <strong class="badge badge-danger">Ticket Price: </strong> ${{ $film->ticket_price }}
            <p>{{ $film->description }}</p>
            <strong>Genres: </strong>

            @foreach($film->genres as $genre)
                <span style="list-style: none">{{ $genre->name }}</span>
            @endforeach

            <hr>
            <a href="{{ route('web.films') }}" class="btn btn-warning">← Back to films</a>

            @include('partials.films.comment')
        @endif
    </div>
@stop
