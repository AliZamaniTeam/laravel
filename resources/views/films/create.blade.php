@extends('layouts.app')
@section('content')

    <div class="container">

        <h2>Create Film</h2>
        <hr>
        {{ Form::open(['method' => 'post', 'files' => true]) }}
        <div class="row">
            <div class="col-md-6">
                <div class="form-row">
                    <div class="form-group col-md-12">
                        {{ Form::label('name', 'Name') }}
                        {{ Form::text('name', null, ['class' => 'form-control', 'id' => 'name']) }}
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-12">
                        {{ Form::label('description', 'Description') }}
                        {{ Form::textarea('description', null, ['class' => 'form-control', 'id' => 'description', 'rows' => 12]) }}
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-row">
                    <div class="form-group col-md-12">
                        {{ Form::label('release_date', 'Release Date') }}
                        {{ Form::date('release_date', \Carbon\Carbon::now(), ['class' => 'form-control', 'id' => 'release_date']) }}
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                {{ Form::label('rating', 'Rating') }}
                                {{ Form::select('rating', range(1, 5), 2, ['class' => 'form-control', 'id' => 'rating']) }}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                {{ Form::label('ticket_price', 'Ticket Price') }}
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">$</div>
                                    </div>
                                    {{ Form::text('ticket_price', null, ['class' => 'form-control', 'id' => 'ticket_price']) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-12">
                        {{ Form::label('genres', 'Genres') }}
                        <select name="genres[]" id="genres" class="form-control multiple" multiple>
                            @foreach($genres as $genre)
                                <option value="{{ $genre->id }}">{{ $genre->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-12">
                        {{ Form::label('country', 'Country') }}
                        {{ Form::text('country', null, ['class' => 'form-control', 'id' => 'country']) }}
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-12">
                        {{ Form::label('photo', 'Photo') }}

                        <div class="input-group mb-3">
                            <div class="custom-file">
                                {{ Form::file('photo', null, ['class' => 'custom-file-input', 'id' => 'photo']) }}
                                <label class="custom-file-label" for="photo">Choose file</label>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            {{ Form::hidden('slug') }}
            {{ Form::submit('Add Film', ['class' => 'btn btn-warning btn-block mx-3']) }}
        </div>

        {{ Form::close() }}
    </div>

@stop


