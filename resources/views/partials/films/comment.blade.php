<div class="comment_form text-left mt-5">
    <h2>Comments</h2>
    <hr>
    @if(count($film->comments))
        @foreach($film->comments as $comment)
            <div class="comment mb-2" id="comment-{{ $comment->id }}" style="border-bottom: 1px #e5e5e5 solid">
                <div class="card">
                    <div class="card-body">
                        <strong>{{ $comment->name }}</strong> has said that :
                        <small class="float-right text-muted">{{ $comment->created_at }}</small>
                        <p class="mt-3">{{ $comment->comment }}</p>
                    </div>
                </div>
            </div>
        @endforeach
    @else
        <div class="alert alert-info text-center">
            Be the first to comment
        </div>
    @endif
    <h4 class="mt-5">Leave a Reply</h4>
    <hr>
    {{ Form::open(['method' => 'post']) }}
    <div class="form-group">
        {{ Form::label('name', 'Name') }}
        {{ Form::text('name', null, ['class' => 'form-control']) }}
    </div>
    <div class="form-group">
        {{ Form::label('comment', 'Comment') }}
        {{ Form::textarea('comment', null, ['class' => 'form-control']) }}
    </div>
    <button type="submit" class="btn btn-primary">Submit Comment</button>
    {{ Form::close() }}
</div>
