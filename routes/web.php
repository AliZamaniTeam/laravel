<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('web.films'));
});

Route::get('films', 'FilmController@index')->name('web.films');
Route::get('film/{slug}', 'FilmController@show')->name('web.film');
Route::post('film/{slug}', 'FilmController@storeComment')->middleware('auth');
Route::get('films/create', 'FilmController@create')->name('web.film.create');
Route::post('films/create', 'FilmController@store');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
